## cobalamin
This container's sole purpose is to demonstrate Kerberized NFS,
i.e. mount the /users directory with sec=krb5p.

If you have trouble mounting the NFS share the first time you run the
playbook, try restarting the container.

Observe how you must login as a user with a password to "unlock" that user's
home directory. Even if you are root and you switch to that user using `su`,
you will not be able to access their files since you do not have a Kerberos
ticket for that user.
