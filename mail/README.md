# Mail container setup
This is one of the trickier ones.

This container depends on coffee, so make sure coffee has been setup first.

## Instructions
First, get the email server up and running:
```
ansible-playbook main.yml
```
At this point, it would be a good idea to send a few emails back
and forth between the dummy users to make sure everything's working
(their usernames are alice, bob, and eve). [Mutt](http://www.mutt.org/)
has already been setup for each user.
```
lxc-attach mail
su - alice
mutt
(send an email to bob@csclub.internal)
exit
su - bob
mutt
(check that alice's email got sent)
```

## Standalone bridge
If you are using a standalone bridge, unfortunately you will not be
able to access the container directly from your physical host because
it is behind a NAT.

Run the following:
```
iptables -t nat -A PREROUTING -s 192.168.122.0/24 -p tcp --dport 80 -j DNAT --to-destination 192.168.100.52
```
Replace '192.168.122.0/24' by the subnet of your VM (your physical host
should also be on this subnet), and replace '192.168.100.52' by the IP
of the mail container.

To make sure this iptables rule is applied automatically at startup,
you can install the iptables-persistent package:
```
apt install iptables-persistent
```
You can use `dpkg-reconfigure iptables-persistent` if you ever need to
change the iptables rules which are applied at startup.

## Shared bridge
If you are using a shared bridge, you can access the container
directly from your physical host, so no extra steps are required.

## Web UI
At some point you will probably want to access the Mailman (2 or 3) web UI.

On your physical host, add the following entry to your `/etc/hosts`:
```
192.168.100.52  mailman.csclub.internal
```
Replace `192.168.100.52` with the IP of the mail container.

## Mailman 3
Mailman 3 will already be installed in the container.

You will also want to create a superuser account:
```
cd /opt/mailman3
su -s /bin/bash www-data
source bin/activate
mailman-web createsuperuser --username bob --email bob@csclub.internal
<enter a password>
exit
```

The csc-general list will have been created by default. If you want
to add any new lists, e.g. syscom:
```
cd /opt/mailman3
su -s /bin/bash list
source bin/activate
mailman create syscom@csclub.internal
```

Now open http://mailman.csclub.internal in your browser, login as bob,
and start doing things. See the official [Mailman 3 documentation](https://docs.mailman3.org)
and [our wiki](https://wiki.csclub.uwaterloo.ca/Mailing_Lists#Mailman_3)
for information on how to create lists, import lists from Mailman 2,
etc. Make sure to send some messages to the lists which you create
to verify that everything's working, e.g. subscribe alice and bob to
csc-general, send a message from bob to csc-general and make sure alice
receives it, etc.

## Mailman 2
The playbook will install Mailman 2 but it will be disabled, since the CSC
has been using Mailman 3 since April 2021.

If you need to use Mailman 2 for whatever reason (e.g. to test a migration),
you will need to edit the Postfix config (specifically, the `alias_maps`
settings in /etc/postfix/main.cf). You will also need to unmask and re-enable
mailman.service. See the Mailman 2 playbook, as well
as the official documentation for Mailman 2 and Postfix, for more details.
