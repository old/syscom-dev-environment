# Outsider container
So this container's a bit special - it represents a host which is **not**
on the UW network. The motivation is to test software which have different
privilege settings for people outside of the local network, e.g. Postfix.

**Note**: I previously gave wrong instructions for forwarding from the
outsider to the other containers. The VM was masquerading connections from
the outsider, which is not what we want. Bring down lxcbr1, then flush the
NAT POSTROUTING chain:
```
ifdown lxcbr1
iptables -t nat -F POSTROUTING
```

We will create a new bridge with a different subnet. Add the following to
your /etc/network/interfaces:
```
auto lxcbr2
iface lxcbr2 inet static
    bridge_ports none
    bridge_fd 0
    bridge_maxwait 0
    address 192.168.101.1/24
    # Forward connections to lxcbr0 and lxcbr1
    up iptables -t nat -C POSTROUTING -s 192.168.101.0/24 -d 192.168.100.0/24 -j ACCEPT 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.101.0/24 -d 192.168.100.0/24 -j ACCEPT
    up iptables -t nat -C POSTROUTING -s 192.168.101.0/24 -d 192.168.101.0/24 -j ACCEPT 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.101.0/24 -d 192.168.101.0/24 -j ACCEPT
    # Masquerade all other connections
    up iptables -t nat -C POSTROUTING -s 192.168.101.0/24 -j MASQUERADE 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.101.0/24 -j MASQUERADE
    down iptables -t nat -D POSTROUTING -s 192.168.101.0/24 -d 192.168.100.0/24 -j MASQUERADE 2>/dev/null || true
    down iptables -t nat -D POSTROUTING -s 192.168.101.0/24 -d 192.168.101.0/24 -j MASQUERADE 2>/dev/null || true
    down iptables -t nat -D POSTROUTING -s 192.168.101.0/24 -j MASQUERADE 2>/dev/null || true
```
Then:
```
ifup lxcbr2
```
Also edit lxcbr1 to look like the following:
```
auto lxcbr1
iface lxcbr1 inet static
    bridge_ports none
    bridge_fd 0
    bridge_maxwait 0
    address 192.168.100.1/24
    # Forward connections to lxcbr0 and lxcbr1
    up iptables -t nat -C POSTROUTING -s 192.168.100.0/24 -d 192.168.100.0/24 -j ACCEPT 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -d 192.168.100.0/24 -j ACCEPT
    up iptables -t nat -C POSTROUTING -s 192.168.100.0/24 -d 192.168.101.0/24 -j ACCEPT 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -d 192.168.101.0/24 -j ACCEPT
    # Masquerade all other connections
    up iptables -t nat -C POSTROUTING -s 192.168.100.0/24 -j MASQUERADE 2>/dev/null || \
       iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -j MASQUERADE
    down iptables -t nat -D POSTROUTING -s 192.168.100.0/24 -d 192.168.100.0/24 -j MASQUERADE 2>/dev/null || true
    down iptables -t nat -D POSTROUTING -s 192.168.100.0/24 -d 192.168.101.0/24 -j MASQUERADE 2>/dev/null || true
    down iptables -t nat -D POSTROUTING -s 192.168.100.0/24 -j MASQUERADE 2>/dev/null || true
```
Then:
```
ifup lxcbr1
```

## Installation
Once you have created the container, edit the following line in
`/var/lib/lxc/outsider/config`:
```
lxc.net.0.link = lxcbr2
```

As usual, chroot into the container and install python3.
Then start it and run the playbook:
```
ansible-playbook main.yml
```
