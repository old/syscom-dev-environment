# auth1
This container provides authentication services to other containers
(LDAP and Kerberos).

## LDAP
Here are some recommended readings for LDAP:
* https://wiki.csclub.uwaterloo.ca/LDAP
* https://wiki.debian.org/LDAP/OpenLDAPSetup
* https://wiki.debian.org/LDAP/NSS
* https://www.openldap.org/doc/admin24/
* https://www.digitalocean.com/community/tutorials/how-to-manage-and-use-ldap-servers-with-openldap-utilities

It is a good idea to become familiar with the `ldapsearch(1)` utility. Here's
a good resource to get started:
https://access.redhat.com/documentation/en-us/red_hat_directory_server/10/html/administration_guide/examples-of-common-ldapsearches

For example, on the CSC servers, you can query information about yourself:
```
ldapsearch -x uid=your_username
```

You can query the UW LDAP servers to see your student info:
```
ldapsearch -x -h uwldap.uwaterloo.ca -b dc=uwaterloo,dc=ca uid=your_username
```

You can also see your Active Directory information:
```
ldapsearch -x -h mixta.teaching.ds.uwaterloo.ca -b 'dc=teaching,dc=ds,dc=uwaterloo,dc=ca' sAMAccountName=your_username
```

Note that we do not use LDAP for authentication; we only use it for NSS
(specifically, passwd and shadow information). For authentication, we use...

## Kerberos
Here are some recommended readings for Kerberos:
https://wiki.csclub.uwaterloo.ca/Kerberos
https://en.wikipedia.org/wiki/Kerberos\_(protocol)
https://web.mit.edu/kerberos/krb5-1.12/doc/index.html

Kerberos is an old, yet still widely-used, protocol for authentication and
single sign-on. You should be familiar with kinit(1), as well as the basic
commands for kadmin(1) (e.g. addprinc, modprinc, ktadd, etc.).

When you login to any of the CSC servers, you should acquire a Kerberos ticket
with kinit(1). This ticket allows you to login to any of the other CSC
machines without having to re-enter your password, as well as a few other
services like using ldapsearch(1) with SASL, sending email, etc.

All of the passwords for the Kerberos principals in this dev environment
are "krb5" (no quotes).

Once you have run the playbook for the auth1 container, make sure you can
login using Kerberos credentials:
```
lxc-attach -n auth1
login
(enter "ctdalek" for username, "krb5" for password)
id
(should show "syscom" as one of the supplementary groups)
kinit
(enter "krb5")
klist
```
The commands above should the Kerberos ticket you just acquired for the
user "ctdalek". You should also be able run sudo, since sudo is configured
via LDAP. Try SSH'ing into some of the other containers using your
Kerberos ticket; you should not be prompted for your password.

If you want to see the keytab entries on a particular host:
```
klist -e -k /etc/krb5.keytab
```

## DNS
It is important for each host to have a PTR record, otherwise SSH GSSAPI
authentication will fail. The most recent version of the DNS playbook
should have PTR records for each host.
