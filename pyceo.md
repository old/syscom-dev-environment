Here are some instructions for getting pyceo (old) working with this
dev environment. Note that some steps might be missing; please open
a PR if you wish to edit these.

Make sure to use the devenv branch as this is where I put my source
code changes:
https://git.csclub.uwaterloo.ca/public/pyceo/src/branch/devenv

Make sure you do a git pull in the dev env repo (the one on GitLab).
When in doubt, git pull.

All instructions below should apply to the containers phosphoric-acid,
coffee and mail, unless specified otherwise. I suggest git cloning
the pyceo repo as one of the dummy user's accounts (e.g. ctdalek)
since their homedirs are mounted on NFS so you can access them
from any container.

1. Try to build ceo (from the pyceo directory):
```
apt install devscripts
debuild -uc -us
```
This will complain about a whole bunch of dependencies missing. Install
all of them, then build again.
1. Install ceo locally as a Python package:
```
apt install --no-install-recommends python-pip python-setuptools
pip2 install -e .
```
1. Create the following symlinks (from the pyceo directory):
```
ln -s $PWD/etc /etc/csc
ln -s $PWD/src /usr/lib/ceod
```
1. Add Kerberos principals for `ceod/<hostname>` in the containers.
For example, in phosphoric-acid:
```
kadmin -p sysadmin/admin
<password is krb5>
addprinc -randkey ceod/phosphoric-acid.csclub.internal
ktadd ceod/phosphoric-acid.csclub.internal
```
Do this for coffee and mail as well. You need to actually be in the
appropriate container when running these commands, since the credentials
are being added to the local keytab.
On phosphoric-acid, you will additionally need to create a principal
called `ceod/admin` (remember to addprinc *and* ktadd).
1. On coffee, run `mysql` as root and run the following:
```sql
CREATE USER ceod@localhost IDENTIFIED BY 'mysql';
GRANT ALL PRIVILEGES ON *.* TO ceod@localhost WITH GRANT OPTION;
```
1. Run `src/ceod` as root on all three containers.
1. Run `bin/ceo` as a regular user (e.g. ctdalek) in any of the containers.
Make sure you can add new members, add new clubs, create club reps, etc.
