---
- hosts: 127.0.0.1
  vars:
    nfs_modules:
      - nfs
      - nfsd
      - rpcsec_gss_krb5
  tasks:
    - name: install dependencies
      apt:
        name: "{{ item }}"
      loop:
        - lxc
        - python3-lxc
    - name: enable IPv4 forwarding
      replace:
        path: /etc/sysctl.conf
        regexp: "^#net.ipv4.ip_forward=1$"
        replace: "net.ipv4.ip_forward=1"
    - name: load sysctl settings
      command: sysctl -p
    - name: load NFS modules
      command: modprobe {{ item }}
      loop: "{{ nfs_modules }}"
    - name: add NFS modules to /etc/modules
      lineinfile:
        path: /etc/modules
        line: "{{ item }}"
      loop: "{{ nfs_modules }}"
    - name: create AppArmor profile
      copy:
        src: templates/lxc-default-with-nfs
        dest: /etc/apparmor.d/lxc/lxc-default-with-nfs
      notify: restart apparmor
    - name: copy /etc/network/interfaces
      template:
        src: templates/interfaces.j2
        dest: /etc/network/interfaces
    - name: bring up bridge interfaces
      shell: ip link show dev {{ item }} | grep UP || ifup {{ item }}
      loop:
        - lxcbr1
        - lxcbr2
    - meta: flush_handlers
    - name: create containers
      # The --no-validate flag is necessary to work around an attack
      # on the SKS keyserver network. See
      # https://discuss.linuxcontainers.org/t/3-0-unable-to-fetch-gpg-key-from-keyserver/2015/15
      # https://github.com/lxc/lxc/issues/3068
      shell: lxc-create -t download -n {{ item }} -- --no-validate -d debian -r buster -a amd64 || true
      loop: "{{ groups.containers }}"
    - name: install python3 in containers
      command:
        cmd: chroot /var/lib/lxc/{{ item }}/rootfs sh -c "dpkg -s python3 || echo nameserver 1.1.1.1 > /etc/resolv.conf && apt update && apt install -y python3"
      loop: "{{ groups.containers }}"
    - name: copy LXC configs
      template:
        src: templates/lxc_config.j2
        dest: /var/lib/lxc/{{ name }}/config
      vars:
        name: "{{ item }}"
        link: "{{ 'lxcbr2' if item == 'outsider' else 'lxcbr1' }}"
      loop: "{{ groups.containers }}"
      register: results
      notify: restart container
    - meta: flush_handlers
    - name: start each container
      shell: lxc-start -n {{ item }} || true
      loop: "{{ groups.containers }}"
  handlers:
    - name: restart apparmor
      systemd:
        name: apparmor
        state: restarted
    - name: restart container
      shell: lxc-stop -n {{ item.item }}; lxc-start -n {{ item.item }}
      loop: "{{ results.results }}"
      when: item.changed

# Everything depends on DNS, so set it up first
- name: set up DNS
  import_playbook: ../dns/main.yml
# Next is auth
- name: set up auth1
  import_playbook: ../auth1/main.yml
# Next is NFS
- name: set up fs00
  import_playbook: ../fs00/main.yml
- name: remount NFS in auth1
  hosts: auth1
  tasks:
    - name: remount /users
      shell: umount /users; mount /users
# Run rest of playbooks
- import_playbook: ../coffee/main.yml
- import_playbook: ../mail/main.yml
- import_playbook: ../phosphoric-acid/main.yml
- import_playbook: ../cobalamin/main.yml
- import_playbook: ../outsider/main.yml
- import_playbook: ../uw00/main.yml
