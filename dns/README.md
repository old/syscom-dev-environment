## DNS container setup
This doesn't actually reflect the way the CSC manages its nameservers,
but the other containers in this repo need some sort of DNS server
to work properly.

## Instructions
```
ansible-playbook main.yml
```
