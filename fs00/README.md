## fs00
This container is meant to emulate the NetApp which exports the /users
directory to the other CSC servers via NFS. Unfortunately we can't
run the real NetApp software inside our container because NetApp uses
a proprietary operating system (ONTAP). So we're going to use the
nfs-kernel-server program instead.

Since NFS runs in the kernel, we're going to need to need some kernel
modules loaded on the LXC host (i.e. the VM). Run the following in
the VM:
```
modprobe nfs
modprobe nfsd
modprobe rpcsec_gss_krb5
```
To make sure these modules automatically get loaded at boot time, add the
following to /etc/modules:
```
nfs
nfsd
rpcsec_gss_krb5
```

We're not ready to start the fs00 container yet - we still need some tweaks
to AppArmor.

## AppArmor
Unfortunately AppArmor does not allow containers to mount NFS shares by
default, so we will create a new AppArmor profile.

Create a new file /etc/apparmor.d/lxc/lxc-default-with-nfs and paste the
following into it:
```
profile lxc-container-default-with-nfs flags=(attach_disconnected,mediate_deleted) {
  #include <abstractions/lxc/container-base>

  deny mount fstype=devpts,
  mount fstype=cgroup -> /sys/fs/cgroup/**,
  mount fstype=cgroup2 -> /sys/fs/cgroup/**,
  mount fstype=nfs*,
  mount fstype=rpc_pipefs,
}
```
Then run:
```
systemctl reload apparmor
```
(Source: [here](https://unix.stackexchange.com/questions/450308/how-to-allow-specific-proxmox-lxc-containers-to-mount-nfs-shares-on-the-network).)

Next, for each container which you have already created (including fs00), add the
following line to its LXC config (should be /var/lib/lxc/container\_name/config):
```
lxc.apparmor.profile = lxc-container-default-with-nfs
```
You will need to restart each container for these changes to take effect.
Also, add this line to /etc/lxc/default.conf so that newly created containers
will have it by default.

Now we are ready to start the fs00 container.

## NFS clients
Once you have re-run the playbooks for each of the other containers, run `mount`
to make sure that /users was mounted correctly. Only phosphoric-acid should
have root access in /users.

## NFS resources
* https://wiki.debian.org/NFSServerSetup
* https://linux.die.net/man/5/exports
* https://linux-nfs.org/wiki/index.php/Main_Page
* http://nfs.sourceforge.net/nfs-howto/
